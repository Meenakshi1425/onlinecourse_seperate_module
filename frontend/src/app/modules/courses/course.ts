export class Course {
  _id: string;
  name: string;
  description: string;
  uri: string;
  startdate: Date;
  enddate: Date;
}
