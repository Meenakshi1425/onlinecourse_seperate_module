export class User {
  public userId:string;
  public username:string;
  public email:string;
  public password:string;
  public is_admin:boolean;
  public courses: any;
}
